#ham tinh tong x + y, sau do neu tong nay trong khoang 15-20 thi xuat ra ket qua, neu tong nay ngam ngoai khoang 15-20 thi xuat ra man hinh 20
def sum(x, y):
    sum = x + y
    if sum in range(15, 20):
        return 20
    else:
        return sum

print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))
